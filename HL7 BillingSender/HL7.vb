﻿Imports System.Text
Imports HL7_BillingSender.Logger
Imports System.Threading
Public Class HL7
    Public MCID As String = String.Empty
    Private _MSG As String = String.Empty
    Public Property MSG() As String
        Get
            Return _MSG
        End Get
        Set(ByVal value As String)
            _MSG = value
        End Set
    End Property

    Public Sub New(ByVal r As HL7_BillingSender.Billing_Interface, ByVal sf As String)
        'Dim ReportType As String = "R"
        'MCID = ReadStep(sf, Logger.stepType.count)
        Dim bTemplate As New StringBuilder
        'MCID += 1
        'MSH
        bTemplate.Append("MSH|^~\&|RESMED|1031|PBRC|1031|")
        bTemplate.Append(Format(Now, "yyyyMMddHHmmss"))
        bTemplate.Append("||DFT^P03|DA")
        'bTemplate.Append(MCID)
        Dim rr As Random = New Random(Format(Now, "hhmmssfff"))
        Dim ss As String = Math.Round(rr.NextDouble, 8).ToString
        bTemplate.Append(ss.Substring(2))
        bTemplate.Append(Format(Now, "hhmmss"))
        bTemplate.Append("|P|2.4")
        bTemplate.Append(Chr(13))
        'EVN
        bTemplate.Append("EVN|P03|")
        bTemplate.Append(Format(Now, "yyyyMMddHHmmss"))
        bTemplate.Append(Chr(13))

        WriteStep(sf, Logger.stepType.count, MCID)

        'append HL7 Template to string
        bTemplate.Append("PID|||^^^^||^|||" + Chr(13))
        bTemplate.Append("PV1|||||||||||||||||||||||||||||||||||||||||||||" + Chr(13))
        bTemplate.Append("FT1|1|||||||||||||||||||^^|^^||||" + Chr(13))
        bTemplate.Append("ZRM|||||||" + Chr(13))

        MSG = bTemplate.ToString

        InsertItem("PID_3_1_ID", r.PID_3_1_ID)
        InsertItem("PID_3_4_Assigning_Authority", r.PID_3_4_Assigning_Authority)
        InsertItem("PID_3_5_Identifier_Type_Code", r.PID_3_5_Identifier_Type_Code)
        InsertItem("PID_5_1_Family_Name", r.PID_5_1_Family_Name)
        InsertItem("PID_5_2_Given_Name", r.PID_5_2_Given_Name)
        InsertItem("PID_7_Date_Time_of_Birth", SafeDate(r.PID_7_Date_Time_of_Birth))
        InsertItem("PID_8_Sex", r.PID_8_Sex)

        InsertItem("PV1_19_Visit_Number", r.PV1_19_Visit_Number)
        InsertItem("PV1_2_Patient_Class", r.PV1_2_Patient_Class)
        InsertItem("PV1_20_Financial_Class", r.PV1_20_Financial_Class)
        InsertItem("PV1_3_Point_of_Care", r.PV1_3_Point_of_Care)
        InsertItem("PV1_44_Admit_Date_Time", SafeDate(r.PV1_44_Admit_Date_Time))
        InsertItem("PV1_45_Discharge_Date_Time", SafeDate(r.PV1_45_Discharge_Date_Time))

        InsertItem("ZRM_1_Request_Referral_Type", r.ZRM_1_Request_Referral_Type)
        InsertItem("ZRM_2_Request_Referral_Letter_Date", SafeDate(r.ZRM_2_Request_Referral_Letter_Date))
        InsertItem("ZRM_3_Referral_Start_Date", SafeDate(r.ZRM_3_Referral_Start_Date))
        InsertItem("ZRM_4_Referral_End_Date", SafeDate(r.ZRM_4_Referral_End_Date))
        InsertItem("ZRM_5_Referral_Period", r.ZRM_5_Referral_Period)
        InsertItem("ZRM_6_LSPN", r.ZRM_6_LSPN)
        InsertItem("ZRM_7_Collection_Point", r.ZRM_7_Collection_Point)

        Dim currF As Int16 = 1
        Dim totalF As Int16 = r.Billing_Interface_FT1s.Count

        For Each f In r.Billing_Interface_FT1s
            InsertItem("FT1" & "|" & currF & "_10_Transaction_Quantity", f.FT1_10_Transaction_Quantity)
            InsertItem("FT1" & "|" & currF & "_12_Transaction_Amount_Unit", f.FT1_12_Transaction_Amount_Unit)
            InsertItem("FT1" & "|" & currF & "_13_Department_Code", f.FT1_13_Department_Code)
            InsertItem("FT1" & "|" & currF & "_20_1_Performed_By_Code_ID", f.FT1_20_1_Performed_By_Code_ID)
            InsertItem("FT1" & "|" & currF & "_20_2_Performed_By_Code_Family_Name", f.FT1_20_2_Performed_By_Code_Family_Name)
            InsertItem("FT1" & "|" & currF & "_20_3_Performed_By_Code_Given_Name", f.FT1_20_3_Performed_By_Code_Given_Name)
            InsertItem("FT1" & "|" & currF & "_21_1_Ordered_by_Code_ID", f.FT1_21_1_Ordered_by_Code_ID)
            InsertItem("FT1" & "|" & currF & "_21_2_Ordered_by_Code_Family_Name", f.FT1_21_2_Ordered_by_Code_Family_Name)
            InsertItem("FT1" & "|" & currF & "_21_3_Ordered_by_Code_Given_Name", f.FT1_21_3_Ordered_by_Code_Given_Name)
            InsertItem("FT1" & "|" & currF & "_23_Filler_Order_Number", f.FT1_23_Filler_Order_Number)
            InsertItem("FT1" & "|" & currF & "_25_Procedure_Code", f.FT1_25_Procedure_Code)
            InsertItem("FT1" & "|" & currF & "_4_Transaction_Date", SafeDate(f.FT1_4_Transaction_Date))
            InsertItem("FT1" & "|" & currF & "_7_Transaction_Code", f.FT1_7_Transaction_Code)
            InsertItem("FT1" & "|" & currF & "_6_Transaction_Type", f.FT1_6_Transaction_Type)

            currF += 1
            If totalF >= currF Then
                MSG = MSG.Insert(MSG.IndexOf("ZRM|"), "FT1|" & currF & "|||||||||||||||||||^^|^^||||" + Chr(13))
            End If
        Next

        Dim GT1 As String = Chr(13) + "GT1|||^^^^||^^^^||||||||||||||||" + Chr(13)      'add extra chr(13) to make the segment recognition logic work

        InsertItem("GT1_2_Workers_Compensation_Claim_Number", r.GT1_2_Workers_Compensation_Claim_Number, GT1)
        InsertItem("GT1_3_1_Debtor_Contact_Name_Family_Name", r.GT1_3_1_Debtor_Contact_Name_Family_Name, GT1)
        InsertItem("GT1_3_2_Debtor_Contact_Name_Given_Name", r.GT1_3_2_Debtor_Contact_Name_Given_Name, GT1)
        InsertItem("GT1_3_3_Debtor_Contact_Name_Other_Name", r.GT1_3_3_Debtor_Contact_Name_Other_Name, GT1)
        InsertItem("GT1_3_5_Debtor_Contact_Name_Title", r.GT1_3_5_Debtor_Contact_Name_Title, GT1)
        InsertItem("GT1_5_1_Debtor_Contact_Address_Street_Address", r.GT1_5_1_Debtor_Contact_Address_Street_Address, GT1)
        InsertItem("GT1_5_2_Debtor_Contact_Address_Other_Address", r.GT1_5_2_Debtor_Contact_Address_Other_Address, GT1)
        InsertItem("GT1_5_3_Debtor_Contact_Address_Suburb", r.GT1_5_3_Debtor_Contact_Address_Suburb, GT1)
        InsertItem("GT1_5_4_Debtor_Contact_Address_State", r.GT1_5_4_Debtor_Contact_Address_State, GT1)
        InsertItem("GT1_5_5_Debtor_Contact_Address_Postcode", r.GT1_5_5_Debtor_Contact_Address_Postcode, GT1)
        InsertItem("GT1_6_Debtor_Contact_Phone_Home", r.GT1_6_Debtor_Contact_Phone_Home, GT1)
        InsertItem("GT1_7_Debtor_Contact_Phone_Work", r.GT1_7_Debtor_Contact_Phone_Work, GT1)
        InsertItem("GT1_10_Debtor_Contact_Type", r.GT1_10_Debtor_Contact_Type, GT1)
        InsertItem("GT1_13_Injury_Date", SafeDate(r.GT1_13_Injury_Date), GT1)
        InsertItem("GT1_21_Debtor_Company_Name", r.GT1_21_Debtor_Company_Name, GT1)
        If GT1 <> Chr(13) + "GT1|||^^^^||^^^^||||||||||||||||" + Chr(13) Then
            MSG = MSG + GT1.Substring(1)        'remove the extra chr(13) before joing strings back toghether
        End If
        ''PID
        'bTemplate.Append("PID|||")
        'bTemplate.Append(r.VPatient_Id)
        'bTemplate.Append("||")
        'bTemplate.Append(r.P_FamilyName)
        'bTemplate.Append("^")
        'bTemplate.Append(r.P_GivenName)
        'bTemplate.Append("^^^")
        'bTemplate.Append(r.P_Prefix)
        'bTemplate.Append("||")
        'bTemplate.Append(Format(r.P_DateTimeOfBirth, "yyyyMMddHHmmss"))
        'bTemplate.Append("|")
        'bTemplate.Append(r.VSex)
        'bTemplate.Append("||^")
        'bTemplate.Append(r.P_Race)
        'bTemplate.Append("|")
        'bTemplate.Append(r.P_StreetAddress)
        'bTemplate.Append("^^")
        'bTemplate.Append(r.P_City)
        'bTemplate.Append("^")
        'bTemplate.Append(r.P_StateOrProvince)
        'bTemplate.Append("^")
        'bTemplate.Append(r.P_PostCode)
        'bTemplate.Append("||")
        'bTemplate.Append(r.VPhone)
        'bTemplate.Append("|")
        'bTemplate.Append(r.VPhoneWork)
        'bTemplate.Append("|")
        'bTemplate.Append("|")
        'bTemplate.Append("|")
        'bTemplate.Append("|")
        'bTemplate.Append("|")
        'bTemplate.Append("|")
        'bTemplate.Append("|")
        'bTemplate.Append(Chr(13))

        ''PV1
        'bTemplate.Append("PV1||")
        'bTemplate.Append(r.V_PatientClass)
        'bTemplate.Append("|")
        'bTemplate.Append(Chr(13))

        ''FT1
        'bTemplate.Append("FT1||")
        'bTemplate.Append(r.F_BillingSessionID)
        'bTemplate.Append("||")
        'bTemplate.Append(Format(r.F_TransactionDate, "yyyyMMddHHmmss"))
        'bTemplate.Append("||")
        'bTemplate.Append("CG")
        'bTemplate.Append("|")
        'bTemplate.Append(r.F_BillingItemCode)
        'bTemplate.Append("|||||||||||||^")
        'bTemplate.Append(r.F_PerformedBy)
        'bTemplate.Append("|")
        'bTemplate.Append(r.F_RequestingMO_Pn.Trim)
        'bTemplate.Append("||")
        '' bTemplate.Append(r.ResultsID) 
        'bTemplate.Append("|")
        '' bTemplate.Append(r.TypedBy)
        'bTemplate.Append("|")
        'bTemplate.Append(r.F_ProcedureCode)
        'bTemplate.Append(Chr(13))

        ''ZPH
        'bTemplate.Append("ZPH|||||")
        'bTemplate.Append(Format(r.Z_RequestDate, "yyyyMMddHHmmss"))
        'bTemplate.Append("|")
        'bTemplate.Append(r.Z_ReferralEndDate)    'where is ref_date???
        'bTemplate.Append(Chr(13))

    End Sub

    Private Sub InsertItem(ByVal ItemName As String, ByVal value As String, Optional ByRef Message As String = Nothing)
        Dim baseMSG As Boolean = False
        If Message Is Nothing Then
            Message = MSG
            baseMSG = True
        End If
        If ItemName IsNot Nothing AndAlso Not String.IsNullOrEmpty(value) AndAlso ItemName.Contains("_") AndAlso ItemName.Length > 5 Then
            'cut up ItemName to find coordinates
            ItemName = ItemName.Trim
            Dim arr = ItemName.Split("_")
            Dim seg = arr(0).ToString
            Dim pipes = CType(arr(1), Integer)
            Dim HasCarrot As Boolean = False
            Dim carrot As Integer
            If IsNumeric(arr(2)) Then
                carrot = CType(arr(2), Integer) - 1
                HasCarrot = True
            End If

            Dim Location As Integer
            If HasCarrot Then
                Location = loopMSGTo("^", carrot, loopMSGTo("|", pipes, Message.LastIndexOf(Chr(13) + seg + "|"), Message), Message)
            Else
                Location = loopMSGTo("|", pipes, Message.LastIndexOf(Chr(13) + seg + "|"), Message)
            End If

            'validate characters
            value = value.Trim.Replace("|", "").Replace("^", "")
            Message = Message.Insert(Location, value)
            If baseMSG Then
                MSG = Message
            End If
        End If
    End Sub
    Private Function loopMSGTo(ByVal delimeter As Char, ByVal i As Integer, ByVal startIndex As Integer, ByRef Message As String) As Integer
        Dim at As Integer = startIndex
        Try
            For f = 0 To i - 1
                at = Message.IndexOf(delimeter, at)
                If at > 0 Then at += 1
            Next
            Return at
        Catch ex As Exception
            Dim hhh = 7384
        End Try
    End Function

    Private Function SafeDate(ByVal strD As String) As String
        Return strD

        'Dim d As Date
        'Dim formats() As String = {"dd/MM/yyyy", "d/MM/yyyy"}
        'Date.TryParse(strD, d)
        'If Date.TryParseExact(strD, formats, Nothing, Globalization.DateTimeStyles.None, d) Then
        '    Dim dd = Format(d, "yyyyMMddHHmmss")

        '    Return dd
        'Else
        '    Return ""
        'End If
    End Function

    'Public Sub New(ByVal r As HL7_BillingSender.RftResult, ByVal p As HL7_BillingSender.Demographic, ByVal sf As String)
    '    Dim ReportType As String = "R"
    '    MCID = ReadStep(sf, Logger.stepType.count)
    '    Dim bTemplate As New StringBuilder
    '    MCID += 1
    '    'MSH
    '    bTemplate.Append("MSH|^~\&|RESMED|1031|PBRC|1031|")
    '    bTemplate.Append(Format(Now, "yyyyMMddHHmmss"))
    '    bTemplate.Append("||DFT^P03|DA")
    '    bTemplate.Append(MCID)
    '    bTemplate.Append(Format(Now, "mmss"))
    '    bTemplate.Append("|P|2.4")
    '    bTemplate.Append(Chr(13))

    '    WriteStep(sf, Logger.stepType.count, MCID)

    '    'PID
    '    bTemplate.Append("PID|||")
    '    bTemplate.Append(p.VPatient_Id)
    '    bTemplate.Append("||")
    '    bTemplate.Append(p.Surname)
    '    bTemplate.Append("^")
    '    bTemplate.Append(p.FirstName)
    '    bTemplate.Append("^^^")
    '    bTemplate.Append(p.Title)
    '    bTemplate.Append("||")
    '    bTemplate.Append(Format(p.DOB, "yyyyMMddHHmmss"))
    '    bTemplate.Append("|")
    '    bTemplate.Append(p.VSex)
    '    bTemplate.Append("||^")
    '    bTemplate.Append(p.Race)
    '    bTemplate.Append("|")
    '    bTemplate.Append(p.Address)
    '    bTemplate.Append("^^")
    '    bTemplate.Append(p.Suburb)
    '    bTemplate.Append("^")
    '    bTemplate.Append(p.State)
    '    bTemplate.Append("^")
    '    bTemplate.Append(p.PostCode)
    '    bTemplate.Append("||")
    '    bTemplate.Append(p.VPhone)
    '    bTemplate.Append("|")
    '    bTemplate.Append(p.VPhoneWork)
    '    bTemplate.Append("|")
    '    bTemplate.Append("|")
    '    bTemplate.Append("|")
    '    bTemplate.Append("|")
    '    bTemplate.Append("|")
    '    bTemplate.Append("|")
    '    bTemplate.Append("|")
    '    bTemplate.Append(Chr(13))

    '    'PV1
    '    bTemplate.Append("PV1||O|")
    '    bTemplate.Append(Chr(13))

    '    'FT1
    '    bTemplate.Append("FT1||")
    '    bTemplate.Append(r.BillingSessionID)
    '    bTemplate.Append("||")
    '    bTemplate.Append(Format(r.TestDate, "yyyyMMddHHmmss"))
    '    bTemplate.Append("||")
    '    bTemplate.Append("CG")
    '    bTemplate.Append("|")
    '    bTemplate.Append(r.VBillingItems)
    '    bTemplate.Append("|||||||||||||^")
    '    bTemplate.Append(r.Scientist)
    '    bTemplate.Append("|")
    '    bTemplate.Append(r.RequestingMO_Pn)
    '    bTemplate.Append("||")
    '    bTemplate.Append(r.ResultsID)
    '    bTemplate.Append("|")
    '    bTemplate.Append(r.TypedBy)
    '    bTemplate.Append("|")
    '    bTemplate.Append(ReportType)
    '    bTemplate.Append(Chr(13))

    '    'ZPH
    '    bTemplate.Append("ZPH|||||")
    '    bTemplate.Append(Format(r.RequestDate, "yyyyMMddHHmmss"))
    '    bTemplate.Append("|")
    '    bTemplate.Append("")    'where is ref_date???
    '    bTemplate.Append(Chr(13))

    '    MSG = bTemplate.ToString
    'End Sub
    'Public Sub New(ByVal s As HL7_BillingSender.SlpResult, ByVal p As HL7_BillingSender.Demographic, ByVal sf As String)
    '    Dim ReportType As String = "S"
    '    MCID = ReadStep(sf, Logger.stepType.count)
    '    Dim bTemplate As New StringBuilder
    '    MCID += 1
    '    'MSH
    '    bTemplate.Append("MSH|^~\&|RESMED|1031|PBRC|1031|")
    '    bTemplate.Append(Format(Now, "yyyyMMddHHmmss"))
    '    bTemplate.Append("||DFT^P03|DA")
    '    bTemplate.Append(MCID)
    '    bTemplate.Append(Format(Now, "mmss"))
    '    bTemplate.Append("|P|2.4")
    '    bTemplate.Append(Chr(13))

    '    WriteStep(sf, Logger.stepType.count, MCID)

    '    'PID
    '    bTemplate.Append("PID|||")
    '    bTemplate.Append(p.VPatient_Id)
    '    bTemplate.Append("||")
    '    bTemplate.Append(p.Surname)
    '    bTemplate.Append("^")
    '    bTemplate.Append(p.FirstName)
    '    bTemplate.Append("^^^")
    '    bTemplate.Append(p.Title)
    '    bTemplate.Append("||")
    '    bTemplate.Append(Format(p.DOB, "yyyyMMddHHmmss"))
    '    bTemplate.Append("|")
    '    bTemplate.Append(p.VSex)
    '    bTemplate.Append("||^")
    '    bTemplate.Append(p.Race)
    '    bTemplate.Append("|")
    '    bTemplate.Append(p.Address)
    '    bTemplate.Append("^^")
    '    bTemplate.Append(p.Suburb)
    '    bTemplate.Append("^")
    '    bTemplate.Append(p.State)
    '    bTemplate.Append("^")
    '    bTemplate.Append(p.PostCode)
    '    bTemplate.Append("||")
    '    bTemplate.Append(p.VPhone)
    '    bTemplate.Append("|")
    '    bTemplate.Append(p.VPhoneWork)
    '    bTemplate.Append(Chr(13))

    '    'PV1
    '    bTemplate.Append("PV1||O|")
    '    bTemplate.Append(Chr(13))

    '    'FT1
    '    bTemplate.Append("FT1||")
    '    bTemplate.Append(s.StudyID) 'billingSessionId
    '    bTemplate.Append("||")
    '    bTemplate.Append(Format(s.StudyDate, "yyyyMMddHHmmss")) 'testdate
    '    bTemplate.Append("||")
    '    bTemplate.Append("CG")
    '    bTemplate.Append("|")
    '    bTemplate.Append(s.VBillingItems)
    '    bTemplate.Append("|||||||||||||^")
    '    bTemplate.Append(s.StudyPerformedBy)    'scientist
    '    bTemplate.Append("|")
    '    bTemplate.Append(s.RequestingMO_ProviderNum)
    '    bTemplate.Append("||")
    '    bTemplate.Append("")    'resultsId
    '    bTemplate.Append("|")
    '    bTemplate.Append(s.TypedBy)
    '    bTemplate.Append("|")
    '    bTemplate.Append(ReportType)
    '    bTemplate.Append(Chr(13))

    '    'ZPH
    '    bTemplate.Append("ZPH|||||")
    '    bTemplate.Append(Format(s.RequestDate, "yyyyMMddHHmmss"))
    '    bTemplate.Append("|")
    '    bTemplate.Append(Format(s.Ref_Date, "yyyyMMddHHmmss"))    'where is ref_date???
    '    bTemplate.Append(Chr(13))

    '    MSG = bTemplate.ToString
    'End Sub

    'Public Sub New(ByVal e As HL7_BillingSender.ExResult, ByVal p As HL7_BillingSender.Demographic, ByVal sf As String)
    '    Dim ReportType As String = "E"
    '    MCID = ReadStep(sf, Logger.stepType.count)
    '    Dim bTemplate As New StringBuilder
    '    MCID += 1
    '    'MSH
    '    bTemplate.Append("MSH|^~\&|RESMED|1031|PBRC|1031|")
    '    bTemplate.Append(Format(Now, "yyyyMMddHHmmss"))
    '    bTemplate.Append("||DFT^P03|DA")
    '    bTemplate.Append(MCID)
    '    bTemplate.Append(Format(Now, "mmss"))
    '    bTemplate.Append("|P|2.4")
    '    bTemplate.Append(Chr(13))

    '    WriteStep(sf, Logger.stepType.count, MCID)

    '    'PID
    '    bTemplate.Append("PID|||")
    '    bTemplate.Append(p.VPatient_Id)
    '    bTemplate.Append("||")
    '    bTemplate.Append(p.Surname)
    '    bTemplate.Append("^")
    '    bTemplate.Append(p.FirstName)
    '    bTemplate.Append("^^^")
    '    bTemplate.Append(p.Title)
    '    bTemplate.Append("||")
    '    bTemplate.Append(Format(p.DOB, "yyyyMMddHHmmss"))
    '    bTemplate.Append("|")
    '    bTemplate.Append(p.VSex)
    '    bTemplate.Append("||^")
    '    bTemplate.Append(p.Race)
    '    bTemplate.Append("|")
    '    bTemplate.Append(p.Address)
    '    bTemplate.Append("^^")
    '    bTemplate.Append(p.Suburb)
    '    bTemplate.Append("^")
    '    bTemplate.Append(p.State)
    '    bTemplate.Append("^")
    '    bTemplate.Append(p.PostCode)
    '    bTemplate.Append("||")
    '    bTemplate.Append(p.VPhone)
    '    bTemplate.Append("|")
    '    bTemplate.Append(p.VPhoneWork)
    '    bTemplate.Append(Chr(13))

    '    'PV1
    '    bTemplate.Append("PV1||O|")
    '    bTemplate.Append(Chr(13))

    '    'FT1
    '    bTemplate.Append("FT1||")
    '    bTemplate.Append(e.BillingSessionID)
    '    bTemplate.Append("||")
    '    bTemplate.Append(Format(e.TestDate, "yyyyMMddHHmmss"))
    '    bTemplate.Append("||")
    '    bTemplate.Append("CG")
    '    bTemplate.Append("|")
    '    bTemplate.Append(e.VBillingItems)
    '    bTemplate.Append("|||||||||||||^")
    '    bTemplate.Append(e.Scientist)
    '    bTemplate.Append("|")
    '    bTemplate.Append(e.RequestingMO_Pn)
    '    bTemplate.Append("||")
    '    bTemplate.Append(e.ResultsID)
    '    bTemplate.Append("|")
    '    bTemplate.Append(e.Typist)
    '    bTemplate.Append("|")
    '    bTemplate.Append(ReportType)
    '    bTemplate.Append(Chr(13))

    '    'ZPH
    '    bTemplate.Append("ZPH|||||")
    '    bTemplate.Append(Format(e.RequestDate, "yyyyMMddHHmmss"))
    '    bTemplate.Append("|")
    '    bTemplate.Append("")    'where is ref_date???
    '    bTemplate.Append(Chr(13))

    '    MSG = bTemplate.ToString
    'End Sub
End Class
