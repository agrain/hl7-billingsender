﻿Imports System.Threading
Imports System.Net.Sockets
Imports HL7_BillingSender.Logger
Public Class CBFactory
    '"Provider=SQLOLEDB.1;Integrated Security=SSPI;Initial Catalog=ResMedCurrent;Data Source=(local)"
    Dim DataSource = "Data Source=AGRAIN\SQLEXPRESS;Initial Catalog=ResMedShell;Integrated Security=True"
    Private t As HL7_BillingSender.Target
    Private l As String 'logfile
    Private sf As String 'stepfile
    Public Sub New(ByVal t As HL7_BillingSender.Target, ByVal sf As String)
        Me.t = t
        Me.l = l
        Me.sf = sf
    End Sub
    Public Sub InitiateFiring()
        Do While (Not Service1.bClosing)          'repeat each minute
            Try
                Dim Last_RftResults As Integer = ReadStep(sf, stepType.rft)
                Dim Last_SlpResults As Integer = ReadStep(sf, stepType.slp)
                Dim Last_exResults As Integer = ReadStep(sf, stepType.ex)
                'check for new rows to send
                Dim DB As New ResMedDataContext
                DB.Connection.ConnectionString = t.connectionString
                'MSGS

                'TEST FOR WHAT HAPPENS WHEN MORE ROWS ARE INTRODUCED
                Dim msg2 = (From r In DB.Billing_Interfaces Where r.Message_Sent Is Nothing Or (r.Message_Sent <> "Y" And r.Message_Sent <> "y") Select r).ToList
                'Logger.LogWrite("MSG Rows ", MSGToFire.Countl)
                For Each m In msg2
                    If Not Service1.bClosing Then
                        LoadRow(m)
                    Else : Exit For
                    End If
                Next

                WriteStep(sf, stepType.LastCheckedDate, Format(Now, "yyyyMMddHHmmss"))
                If t IsNot Nothing Then
                    If t.client IsNot Nothing Then
                        t.client.Close()
                    End If
                End If
            Catch ex As Exception
                Logger.LogWrite("ERROR " & ex.ToString, ex.Message)
            End Try
            If Not Service1.bClosing Then Thread.Sleep(t.LoopTime)
        Loop
    End Sub

    Private Sub LoadRow(ByVal o As Billing_Interface)
        Try
            Dim DB As New ResMedDataContext
            DB.Connection.ConnectionString = t.connectionString
            o = (From mes In DB.Billing_Interfaces Where mes.id = o.id Select mes).FirstOrDefault
            Dim m = New HL7(o, sf)
            Logger.LogWrite(o.id.ToString, m.MSG) 'rollover the log file each day changing the old one to a datestamped version and continuing in a new one
            'send messages
            Service1.WaitConnect(t)
            Logger.LogWrite("Connected to Port:" & t.IPAddress.ToString & " Port: " & t.port.ToString, Nothing)
            Logger.LogWrite("Sending Message:" & m.MCID, Nothing)

            'create new message log
            Dim MsgBlob As New XElement(<message></message>)
            MsgBlob.Value = m.MSG
            Dim AckBlob As New XElement(<ack></ack>)
            Dim ml As New HL7_BillingSender.Billing_Interface_Log With {.Billing_Interface = o, .MessageBlob = MsgBlob}
            o.Message_Sent = "Y"
            o.Last_Sent_Date = Now
            ml.MessageSentDate = Now

            Dim reciept = Sender.socketSendReceive(t.client, m.MSG)
            If reciept <> "" Then
                Logger.LogWrite("Message Accepted:" & m.MCID, Nothing)
                'update the last message for the reciepts
                WriteStep(sf, stepType.rft, o.id)
                WriteStep(sf, stepType.count, m.MCID)

                'update message log status
                ml.AckStatus = "A"
                ml.AckReceivedDate = Now
            Else
                ml.AckStatus = "N"
            End If
            ml.AckReceivedDate = Now
            AckBlob.Value = reciept
            ml.AckBlob = AckBlob

            DB.Billing_Interface_Logs.InsertOnSubmit(ml)
            DB.SubmitChanges()

        Catch ex As Exception
            Logger.LogWrite("ERROR " & ex.ToString, ex.Message)
        End Try
    End Sub

    'Private Sub LoadRow(ByVal o As RftResult)
    '    Try
    '        Dim DB As New ResMedDataContext
    '        DB.Connection.ConnectionString = t.connectionString

    '        Dim P = (From d In DB.Demographics Where d.PatientID = o.PatientID).FirstOrDefault
    '        If P IsNot Nothing Then
    '            Dim m = New HL7(o, P, sf)
    '            Logger.LogWrite("RFT " & o.ResultsID.ToString, m.MSG) 'rollover the log file each day changing the old one to a datestamped version and continuing in a new one
    '            'send messages
    '            If t.client.Client Is Nothing Then
    '                t.client = New TcpClient(t.IPAddress, t.port)
    '            ElseIf Not t.client.Connected Then
    '                t.client = New TcpClient(t.IPAddress, t.port)
    '            End If
    '            If Sender.socketSendReceive(t.client, m.MSG) <> "" Then
    '                'update the last message for the reciepts
    '                WriteStep(sf, stepType.rft, o.ResultsID)
    '                WriteStep(sf, stepType.count, m.MCID)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Logger.LogWrite("ERROR " & ex.ToString, ex.Message)
    '    End Try
    'End Sub
    'Private Sub LoadRow(ByVal o As SlpResult)
    '    Dim DB As New ResMedDataContext
    '    DB.Connection.ConnectionString = t.connectionString
    '    Try
    '        Dim P = (From d In DB.Demographics Where d.PatientID = o.PatientID).FirstOrDefault
    '        If P IsNot Nothing Then
    '            Dim m = New HL7(o, P, sf)
    '            Logger.LogWrite("SLP " & o.StudyID.ToString, m.MSG)
    '            'send messages
    '            If t.client.Client Is Nothing Then
    '                t.client = New TcpClient(t.IPAddress, t.port)
    '            ElseIf Not t.client.Connected Then
    '                t.client = New TcpClient(t.IPAddress, t.port)
    '            End If
    '            If Sender.socketSendReceive(t.client, m.MSG) <> "" Then
    '                'update the last message for the reciepts
    '                WriteStep(sf, stepType.slp, o.StudyID)
    '                WriteStep(sf, stepType.count, m.MCID)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Logger.LogWrite("ERROR " & ex.ToString, ex.Message)
    '    End Try
    'End Sub
    'Private Sub LoadRow(ByVal o As ExResult)
    '    Try
    '        Dim DB As New ResMedDataContext
    '        DB.Connection.ConnectionString = t.connectionString

    '        Dim P = (From d In DB.Demographics Where d.PatientID = o.PatientID).FirstOrDefault
    '        If P IsNot Nothing Then
    '            Dim m = New HL7(o, P, sf)
    '            Logger.LogWrite("EX " & o.ResultsID.ToString, m.MSG)
    '            'send messages
    '            If t.client.Client Is Nothing Then
    '                t.client = New TcpClient(t.IPAddress, t.port)
    '            ElseIf Not t.client.Connected Then
    '                t.client = New TcpClient(t.IPAddress, t.port)
    '            End If
    '            If Sender.socketSendReceive(t.client, m.MSG) <> "" Then
    '                'update the last message for the reciepts
    '                WriteStep(sf, stepType.ex, o.ResultsID)
    '                WriteStep(sf, stepType.count, m.MCID)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Logger.LogWrite("ERROR " & ex.ToString, ex.Message)
    '    End Try
    'End Sub
End Class
