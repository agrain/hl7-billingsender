﻿Imports System
Imports System.IO
Imports System.Text
Imports System.Threading
Public Class Logger
    Public Enum stepType
        rft
        slp
        ex
        count
        LastCheckedDate
    End Enum
    Public Shared Sub LogWrite(ByVal message As String, ByVal szHl7 As String)
        If My.Settings.logfile = "" Then Exit Sub
        If message.Contains("ERROR") Then   'check log for duplicate error messages
            Dim wholeLog As String = File.ReadAllText(My.Settings.logfile)
            If wholeLog.Contains(szHl7) Then Exit Sub 'do not log duplicate messages
        End If

        Dim FDate = IO.File.GetLastWriteTime(My.Settings.logfile)
        If FDate < Now.AddDays(-1) Then
            Dim newl As String = My.Settings.logfile.Replace("HL7BSLog.txt", "HL7BSLog_" & Format(FDate, "yyyyMMdd") & ".txt")
            Try
                Dim Oexists As Boolean = System.IO.File.Exists(My.Settings.logfile)
                Dim Nexists As Boolean = System.IO.File.Exists(newl)
                System.IO.File.Move(My.Settings.logfile, newl)
            Catch ex As Exception
                System.IO.File.Move(My.Settings.logfile, newl.Replace(".txt", "A.txt"))
            End Try
        End If

        Dim fp As Integer = FreeFile()
        Try
            FileOpen(fp, My.Settings.logfile, OpenMode.Append, OpenAccess.ReadWrite, OpenShare.Shared)

            PrintLine(fp, Format(Now, "d/MMM/yyyy HH:mm") & " - " & message)
            If szHl7 <> "" Then
                Dim segArray() As String = Split(szHl7, Chr(13))
                Dim seg As String
                For Each seg In segArray
                    seg = Replace(seg, Chr(10), "")
                    PrintLine(fp, Chr(9) & seg)
                Next
            End If
        Catch ex As Exception
            Dim ak = 11
        End Try
        Try
            FileClose(fp)
        Catch ex As Exception
        End Try
    End Sub


    Public Shared Function ReadStep(ByVal sf As String, ByVal ty As stepType) As Integer
        Try
            If sf = "" Then Exit Function
            Dim ret As Integer = 0
            Dim marker As String = String.Empty
            Select Case ty
                Case stepType.rft : marker = "r:"
                Case stepType.slp : marker = "s:"
                Case stepType.ex : marker = "e:"
                Case stepType.count : marker = "c:"
                Case stepType.LastCheckedDate : marker = "d:"
                Case Else
            End Select
            Dim steps As String = File.ReadAllText(sf)
            If steps.Length > 0 Then
                steps = steps.Substring(steps.IndexOf(marker) + 2, steps.IndexOf(",", steps.IndexOf(marker)) - (steps.IndexOf(marker) + 2)).Trim
                If IsNumeric(steps) Then ret = CType(steps, Integer)
            Else
                File.WriteAllText(sf, "r:0,s:0,e:0,c:0,d:20110101,")
            End If
            Return ret
        Catch ex As Exception
            Logger.LogWrite("ERROR - ReadStep " & ex.ToString, ex.Message)
        End Try
    End Function
    Public Shared Sub WriteStep(ByVal sf As String, ByVal ty As stepType, ByVal v As String)
        Try
            If sf = "" Then Exit Sub
            Dim ret As Integer = 0
            Dim marker As String = String.Empty
            Select Case ty
                Case stepType.rft : marker = "r:"
                Case stepType.slp : marker = "s:"
                Case stepType.ex : marker = "e:"
                Case stepType.count : marker = "c:"
                Case stepType.LastCheckedDate : marker = "d:"
                Case Else
            End Select
            Dim steps As String = File.ReadAllText(sf)
            If steps.Length > 0 Then
                Dim replacer = steps.Substring(steps.IndexOf(marker) + 2, steps.IndexOf(",", steps.IndexOf(marker)) - (steps.IndexOf(marker) + 2)).Trim
                steps = steps.Replace(marker & replacer, marker & v)
            End If
            File.WriteAllText(sf, steps)
        Catch ex As Exception
            Logger.LogWrite("ERROR " & ex.ToString, ex.Message)
        End Try
    End Sub
End Class
