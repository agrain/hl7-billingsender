﻿Imports System.Net.Sockets
Imports System.Threading
Imports Microsoft.Win32
Public Class Service1

#Region "Vars"
    Private Const cszRegistry As String = "SYSTEM\CurrentControlSet\Services\DataAgility\HL7BillingSender"
    Private Const cszListenPort As String = "ListenPort"
    Private Const cszLogFile As String = "LogFile"
    Private Const cszStepFile As String = "StepFile"
    Private Const cszQueueName As String = "QueueName"
    Private Const cszConnectionString = "ConnectionString"
    Private Const cszQueryConnectionString = "QueryConnectionString"
    'Private Const cszStoredProc = "StoredProcedure"
    Private Const cszTargetIP As String = "TargetIP"
    Private Const cszTargetPort As String = "TargetPort"
    Private Const cszSendTimeout As String = "SendTimeout"
    Private Const cszQueryPort As String = "QueryPort"
    Private Const cszLoopTime As String = "LoopTime"

    Private Shared threadListen As Thread
    Private Shared threadQueryListen As Thread
    Private Shared ListenPort As Int32 = 21110
    ' Private Shared QueryPort As Int32 = 0
    Private Shared QueryConnectionString As String = ""
    Private Shared listen As TcpListener
    Private Shared qlisten As TcpListener
    Private Shared lockReceivers As New AutoResetEvent(True)
    Public Shared StepFile As String = ""
    Private Shared bClosingDown As Boolean = False
    Public tar As New Target
    Public Shared targets() As Target = Nothing
    Private Shared bWaitDebug As Boolean = True
    Public Shared bClosing As Boolean = False
    Public Shared LoopTime As Integer = 10000
#End Region

    Protected Overrides Sub OnStart(ByVal args() As String)
        bWaitDebug = True
        'While bWaitDebug
        '    Thread.Sleep(1000)
        'End While

        Dim Fact As CBFactory
        Dim rk As RegistryKey = Registry.LocalMachine.CreateSubKey(cszRegistry)
        If Not IsNothing(rk) Then
            ListenPort = rk.GetValue(cszListenPort, ListenPort)
            My.Settings.logfile = rk.GetValue(cszLogFile, "C:\Program Files\Data Agility\HL7BillingSender\HL7BSLog.txt")
            My.Settings.Save()
            StepFile = rk.GetValue(cszStepFile, StepFile)
            tar.connectionString = rk.GetValue(cszConnectionString, "")
            tar.IPAddress = rk.GetValue(cszTargetIP, "localhost")
            tar.port = rk.GetValue(cszTargetPort, 21110)
            tar.LoopTime = rk.GetValue(cszLoopTime, LoopTime)
            tar.t = Nothing
            If tar.connectionString <> "" Then
                Fact = New CBFactory(tar, StepFile)
                tar.t = New Thread(AddressOf Fact.InitiateFiring)
                tar.t.Start()
            End If
            rk.Close()
        End If
        Logger.LogWrite("HL7BillingSender Service STARTED!", "")
    End Sub

    ''' <summary>
    ''' Holds service in loop until an output connection can be formed
    ''' </summary>
    ''' <param name="tar"></param>
    ''' <remarks></remarks>
    Public Shared Sub WaitConnect(ByRef tar As Target)
        Try
            Do While Not safeClient(tar.client)
                Try
                    tar.client = New TcpClient(tar.IPAddress, tar.port)
                Catch ex As Exception
                    tar.client = Nothing
                    Thread.Sleep(5000)
                    Logger.LogWrite("ERROR " & ex.ToString, ex.Message)
                End Try
            Loop
        Catch ex As Exception
            Logger.LogWrite("ERROR " & ex.ToString, ex.Message)
        End Try
    End Sub

    Private Shared Function safeClient(ByRef c As TcpClient) As Boolean
        If c Is Nothing Then : Return False
        ElseIf c.Client Is Nothing Then : Return False
        ElseIf c.Connected = False Then : Return False
        Else : Return True
        End If
    End Function

    Protected Overrides Sub OnStop()
        bClosing = True
        ' Add code here to perform any tear-down necessary to stop your service.
        Logger.LogWrite("HL7BillingSender Service STOPPED!", "")
        Thread.Sleep(500)
        tar.t.Abort()
    End Sub
End Class
